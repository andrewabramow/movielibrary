﻿/*
Что нужно сделать

Используя модель данных из задачи №1, создайте JSON-документ,
но теперь уже для пяти различных кинолент. Ключи в этой
JSON-базе данных должны быть названиями фильмов, а значениями
служить уже словари с информацией о картинах.

Загрузите базу данных в C++-программу для последующего анализа
. Предусмотрите следующий функционал: поиск актёра по имени
(или фамилии) и вывод в консоль названий кинолент, в которых
снимался актёр, и роль, которую он исполнял.

Что оценивается

Валидность созданного JSON-документа с базой данных о фильмах.
Корректность работы программы поиска актёра.
*/

#include <fstream>
#include <string>
#include "MovieLibrary.h"
#include "nlohmann/json.hpp"

int main()
{
	std::ifstream blue_velvet("blue_velvet.json");
	std::ifstream twin_peaks("twin_peaks.json");
	std::ifstream eraserhead("eraserhead.json");
	std::ifstream lost_highway("lost_highway.json");
	std::ifstream mulholland_Dr("mulholland_Dr.json");

	nlohmann::json movie1;
	nlohmann::json movie2;
	nlohmann::json movie3;
	nlohmann::json movie4;
	nlohmann::json movie5;

	blue_velvet >> movie1;
	twin_peaks >> movie2;
	eraserhead >> movie3;
	lost_highway >> movie4;
	mulholland_Dr >> movie5;

	nlohmann::json movie_lib = {
		{"blue_velvet",movie1},
		{"twin_peaks",movie2},
		{"eraserhead",movie3},
		{"lost_highway",movie4},
		{"mulholland_Dr",movie5}
	};

	std::ofstream out("movie_library.json");
	out << movie_lib;

	std::string name{ "Kyle" };
	std::string surname{ "MacLachlan" };

	for (auto it = movie_lib.begin(); it != movie_lib.end(); ++it) {
		for (auto itt = movie_lib[it.key()].begin(); itt != movie_lib[it.key()].end(); ++itt) {
			std::string temp = itt.key();
			std::size_t found_name = temp.find(name);
			std::size_t found_surname = temp.find(surname);
			if (found_name != std::string::npos ||
				found_surname != std::string::npos) {
				std::cout << it.key() << std::endl;
				std::cout << itt.value() << std::endl;
				break;
			}
		}
	}
	return 0;
}
